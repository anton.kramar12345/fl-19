import { dictionary } from './dictionary.js';

let randomIndex = Math.floor(Math.random() * dictionary.length)
let secret = dictionary[randomIndex]
let startAt = ''
let storage = []
let block = document.getElementById('block')
createBlock()
newBlock()
window.addEventListener('keydown', handleKeyDown)


function createAt(row, attempt, isCurrent) {
  for (let i = 0; i < 5; i++) {
    let cell = row.children[i]
    if (attempt[i] !== undefined) {
      cell.textContent = attempt[i]
    } else {
      cell.innerHTML = '<div style="opacity: 0">X</div>'
    }
    if (isCurrent) {
      cell.style.backgroundColor = 'white'
    } else {
      cell.style.backgroundColor = createBg(attempt, i)
    }
  }
}

function createBg(attempt, i) {
  let correctLetter = secret[i]
  let attemptLetter = attempt[i]
  if (
    attemptLetter === undefined ||
    secret.indexOf(attemptLetter) === -1
  ) {
    return 'white'
  }
  if (correctLetter === attemptLetter) {
    return '#538d4e'
  }
  return '#b59f3b'
}

function handleKeyDown(e) {
  let letter = e.key.toLowerCase()
  if (letter === 'enter') {
    if (startAt.length < 5) {
      return
    }
    if (!dictionary.includes(startAt)) {
      alert('Wrong word')
      return
    }
    storage.push(startAt)
    startAt = ''
  } else if (letter === 'backspace') {
    startAt = startAt.slice(0, startAt.length - 1)
  } else if (/[a-z]/.test(letter)) {
    if (startAt.length < 5) {
      startAt += letter
    }
  }
  newBlock()
}

function createBlock() {
  for (let i = 0; i < 6; i++) {
    let row = document.createElement('div')
    for (let j = 0; j < 5; j++) {
      let cell = document.createElement('div')
      cell.className = 'cell'
      cell.textContent = ''
      row.appendChild(cell)
    }
    block.appendChild(row)
  }
}


function newBlock() {
  let row = block.firstChild
  for (let attempt of storage) {
    createAt(row, attempt, false)
    row = row.nextSibling
  }
  createAt(row, startAt, true)
}
