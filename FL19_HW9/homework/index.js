// #1
function calculateSum(arr) {
    let r=0;
    for(let i=0; i<arr.length; i++) {
      r=r+arr[i];
    }
    return r;
}

console.log(calculateSum([1,2,3,4,5])); //15

// #2
function isTriangle(a, b, c) {
    let result = [a, b, c].sort();
    return result[2] < result[0] + result[1];
}

console.log(isTriangle(5,6,7)); //true
console.log(isTriangle(2,9,3)); //false

// #3
function isIsogram(word) {
return new Set(word.toLowerCase()).size===word.length
}

console.log(isIsogram('Dermatoglyphics')); //true
console.log(isIsogram('abab')); //false

// #4
function isPalindrome(word) {
    let c = word.length;
    let b = Math.floor(c/2);

    for ( let i = 0; i < b; i++ ) {
        if (word[i] !== word[c - 1 - i]) {
            return false;
        }
    }

    return true;
}

console.log(isPalindrome('Dermatoglyphics')); //false
console.log(isPalindrome('abbabba')); //true

// #5
function showFormattedDate(dateObj) {
    let strArray=['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let d = dateObj.getDate();
    let m = strArray[dateObj.getMonth()];
    let y = dateObj.getFullYear();
    return '' + (d <= 9 ? '0' + d : d) + ' of' + ` ` + m + ',' + y;
}

console.log(showFormattedDate(new Date('05/12/22'))); //'12 of May, 2022'

// #6
const letterCount = (str, letter) => {
    let letter_Count = 0;
    for (let position = 0; position < str.length; position++) {
       if (str.charAt(position) === letter) {
         letter_Count += 1;
         }
     }
     return letter_Count;
}

console.log(letterCount('abbaba', 'b')); //3

// #7
function countRepetitions(arr) {
    const obj = {};
    for (let i of arr) { 
        obj[i] ? obj[i]++ : obj[i] = 1
    }
    return obj;
}

console.log(countRepetitions(['banana', 'apple', 'banana'])); // { banana: 2, apple: 1 }

// #8
function calculateNumber(arr) {
   return parseInt(arr.join(""), 2);
}

console.log(calculateNumber([0, 1, 0, 1])); //5
console.log(calculateNumber([1, 0, 0, 1])); //9