// #1
let a;
function extractCurrencyValue(param) {
  a = parseInt(param);
  let b = BigInt(a);
  if(param.length>16){
      return b;
  }else{
    return a;
  } 
}

console.log(extractCurrencyValue('120 USD')); // 120
console.log(extractCurrencyValue('1283948234720742 EUR')); // 1283948234720742n


// #2

let object = {
    name: 'Ann',
    age: 16,
    hobbies: undefined,
    degree: null,
    isChild: false,
    isTeen: true,
    isAdult: false
}

function clearObject(obj) { 
for (let i in obj){
    if(!obj[i]){
        delete obj[i];
    }
}
return obj;
}

console.log(clearObject(object)); // { name: 'Ann', age: 16, isTeen: true }


// #3
let b;
function getUnique(param) {
b = Symbol("'" + param + "'");
return b;
} 

console.log(getUnique('Test')) // Symbol('Test')


// #4
let count;
let d;
let w;
let m;
function countBetweenTwoDays(startDate, endDate) {
    
count = new Date(new Date(endDate).getTime()-new Date(startDate).getTime()).getTime();
    d = parseInt(count/ (1000 * 60 * 60 * 24));
    w = parseInt(d / 7);
    m = parseInt(d / 24);
    console.log(`The difference between dates is: ${d} day(-s), ${w} week(-s), ${m} month(-s)`);
}

countBetweenTwoDays('03/22/22', '05/25/22'); // The difference between dates is: 64 day(-s), 9 week(-s), 2 month(-s)


// #5

function filterArray(arr) {
return arr.filter((value,index) => arr.indexOf(value) === index);
}

console.log(filterArray([1, 2, 2, 4, 5, 5, 5, 6, 6, 7, 7, 8, 8, 8, 9])); // [1, 2, 3, 4, 5, 6, 7, 8, 9]