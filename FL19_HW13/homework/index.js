window.addEventListener('DOMContentLoaded', () => {
    const tile = Array.from(document.querySelectorAll('.tile'));
    const playerDisplay = document.querySelector('.display-player');
    const resetButton = document.querySelector('#reset');
    const announcer = document.querySelector('.announcer');

    const playerOwon = 'wonO';
    const TIE = 'TIE';
    let cell = ['', '', '', '', '', '', '', '', ''];
    let player = 'X';
    let isGameActive = true;
     const PlayerXwon= 'wonX';
    
    const announce = (type) => {
        switch(type){
            case playerOwon:
                announcer.innerHTML = 'Player <span class="playerO">O</span> Won';
                break;
            case PlayerXwon:
                announcer.innerHTML = 'Player <span class="playerX">X</span> Won';
                break;
            case TIE:
                announcer.innerText = 'Tie';
        }
        announcer.classList.remove('hide');
    };

    const isValidAction = (tile) => {
        if (tile.innerText === 'X' || tile.innerText === 'O'){
            return false;
        }

        return true;
    };

    const updateBoard = (index) => {
        cell[index] = player;
    }

    const changePlayer = () => {
        playerDisplay.classList.remove(`player${player}`);
        player = player === 'X' ? 'O' : 'X';
        playerDisplay.innerText = player;
        playerDisplay.classList.add(`player${player}`);
    }

    const userAction = (tile, index) => {
        if(isValidAction(tile) && isGameActive) {
            tile.innerText = player;
            tile.classList.add(`player${player}`);
            updateBoard(index);
            createGame();
            changePlayer();
        }
    }

    function createGame() {
        let roundWon = false;
        for (let i = 0; i <= 7; i++) {
            const endGame = stopGame[i];
            const a = cell[endGame[0]];
            const b = cell[endGame[1]];
            const c = cell[endGame[2]];
            if (a === '' || b === '' || c === '') {
                continue;
            }
            if (a === b && b === c) {
                roundWon = true;
                break;
            }
        }

    if (roundWon) {
            announce(player === 'X' ? PlayerXwon : playerOwon);
            isGameActive = false;
            return;
        }

    if (!cell.includes('')) {
 announce(TIE); 
}
    }
    
    const resetCell= () => {
        cell = ['', '', '', '', '', '', '', '', ''];
        isGameActive = true;
        announcer.classList.add('hide');

        if (player === 'O') {
            changePlayer();
        }

        tile.forEach(tile => {
            tile.innerText = '';
            tile.classList.remove('playerX');
            tile.classList.remove('playerO');
        });
    }

    tile.forEach( (tile, index) => {
        tile.addEventListener('click', () => userAction(tile, index));
    });

    resetButton.addEventListener('click', resetCell);
});

const stopGame = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
];