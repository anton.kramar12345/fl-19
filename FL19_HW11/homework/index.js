//1.
function getWeekDay(date) {
    const dayOfWeek = new Date(date).getDay();    
    return isNaN(dayOfWeek) ? null : 
      ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][dayOfWeek];
  }

console.log(getWeekDay(Date.now()));
console.log(getWeekDay(new Date(2020, 9, 22)));

//2.
function getAmountDaysToNewYear(days){
    let now = new Date();
    let start = new Date(now.getFullYear(), 0, 0);
    let diff = now - start;
    let oneDay = 1000 * 60 * 60 * 24;
    days = Math.floor(diff / oneDay);
    return days;
}

console.log(getAmountDaysToNewYear());

//3.
const birthday17 = new Date(2004, 12, 29);
const birthday15 = new Date(2006, 12, 29);
const birthday22 = new Date(2000, 9, 22);

function getApproveToPass(DOB) {
  let today = new Date();
  let birthDate = new Date(DOB);
  let age = today.getFullYear() - birthDate.getFullYear();  
   if(18 - age === 1 && age < 18){
    return'Hello adventurer, you are to yang for this quest wait for few more months!';
  } else if (age < 18 && 18 - age > 1){
return 'Hello adventurer, you are to yang for this quest wait for 3 years more!';
  } else if(age >= 18){
    return ' Hello adventurer, you may pass!';
  }
  
}
console.log(getApproveToPass(birthday17));
console.log(getApproveToPass(birthday15));
console.log(getApproveToPass(birthday22));


//4.
const elementP = 'tag="div" class="text" style={color: #aeaeae;} value="Hello World!"';  
function transformStringToHtml(string) {  
    const [tagName] = string.match(new RegExp('(?<=tag=")[^"]+(?=")', 'g'));  
    const [tagValue] = string.match(new RegExp('(?<=value=")[^"]+(?=")', 'g'));  
    const tagAttributes = string.replace(/(tag=".*?")/g, '').replace(/({|})/g, '"').replace(/(value=".*?")/g, '');  
    return `<${tagName} ${tagAttributes}>${tagValue}</${tagName}>`;  
}  

console.log(transformStringToHtml(elementP));

//5.
function isValidIdentifier(text) {
  return /^[A-z_$][A-z_\d$]*$/g.test(text);

}

console.log(isValidIdentifier('myVar!'));
console.log(isValidIdentifier('myVar$ '));
console.log(isValidIdentifier('myVar_1'));
console.log(isValidIdentifier('1_myVar'));

//6.

function capitalize(str) {
    return str
        .toLowerCase()
        .split(' ')
        .map(function(word) {
            return word[0].toUpperCase() + word.substr(1);
        })
        .join(' ');
     }
     const testStr = 'My name is John Smith. I am 27.';
     console.log(capitalize(testStr));

//7.
function validatePassword(pw) {

    return /[A-Z]/ .test(pw) &&
           /[a-z]/ .test(pw) &&
           /[0-9]/ .test(pw) &&
           /[a-zA-Z0-9]{8,}/.test(pw) &&
           pw.length > 4;

}
console.log(validatePassword('agent007'));
console.log(validatePassword('AGENT007'));
console.log(validatePassword('AgentOOO'));
console.log(validatePassword('Age_007'));
console.log(validatePassword('Agent007'));

//8.
function bubbleSort(arr) {
    let len = arr.length;
  
    for (let i = 0; i < len ; i++) {
      for(let j = 0 ; j < len - i - 1; j++){ // this was missing
      if (arr[j] > arr[j + 1]) {
        // swap
        let temp = arr[j];
        arr[j] = arr[j+1];
        arr[j + 1] = temp;
      }
     }
    }
    return arr;
  }
  console.log(bubbleSort([7,5,2,4,3,9]));
  //9.
  const inventory = [
    { name: 'milk', brand: 'happyCow', price: 2.1 }, 
    { name: 'chocolate', brand: 'milka', price: 3 }, 
    { name: 'beer', brand: 'hineken', price: 2.2 }, 
    { name: 'soda', brand: 'coca-cola', price: 1 } 
    ];
    function sortByItem(obj) {
        obj.array.sort((a,b) => {
                if (a[obj.item] < b[obj.item]) {
 return -1; 
}
                if (a[obj.item] >b[obj.item]) {
 return 1; 
}
                 return 0;
        });
        return obj.array;
    }
    console.log(sortByItem({item: 'name', array: inventory}));