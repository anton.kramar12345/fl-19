let user,clicksAmount = document.getElementById('clicked');
const start = document.getElementById('start');
const bestResult = document.getElementById('best-result');
const bestResultAllTime = document.getElementById('best-result-all-time');
const clearBestResult = document.getElementById('clear-best-result');
const clearBestResultAllTime = document.getElementById('clear-best-result-all-time');
const clicker = document.getElementById('clicker');
const username = document.getElementById('nickname');
const login = document.getElementById('login');
const gameButtonsGroup = document.querySelectorAll('.buttons button');
const nameUserBlock = document.querySelector('.user-name');
const showUserName = document.querySelector('.user-name h1');


username.addEventListener('input', function () {
	if (
		!this.value ||
		/(\s|[~`!@#%^*+\]{}\\:;?|><="'\\\-а-яА-ЯіІїЇ])/g.test(this.value)
	) {
		login.setAttribute('disabled', 'disabled');
	} else {
		
		login.removeAttribute('disabled', 'disabled');
	}
});

login.addEventListener('click', () => {
	try {
		if (
			!username.value ||
			/(\s|[~`!@#%^*+\]{}\\:;?|><="'\\\-а-яА-ЯіІїЇ])/g.test(
				username.value
			)
		) {
			throw Error(
				
			);
		}
	} catch (err) {
		console.log(err);
	}
	if (
		!username.value ||
		/(\s|[~`!@#%^*+\]{}\\:;?|><="'\\\-а-яА-ЯіІїЇ])/g.test(
			username.value
		)
	) {
	console.log(username);
	} else {
		
		nameUserBlock.style.display = 'flex';
		clicksAmount.textContent = '0';
		showUserName.textContent = username.value;
		gameButtonsGroup.forEach((button) =>
			button.removeAttribute('disabled', 'disabled')
		);
		user = new CreateGame(username.value);
		user.newUser();
	}
});



start.addEventListener('click', () => {
	user = new CreateGame(user.currentUser[0].username);
	gameButtonsGroup.forEach((button) =>
		button.setAttribute('disabled', 'disabled')
	);
	clicksAmount.textContent = '0';
	clicker.removeAttribute('disabled', 'disabled');
	setTimeout(() => {
		clicker.setAttribute('disabled', 'disabled');
		gameButtonsGroup.forEach((button) =>
			button.removeAttribute('disabled', 'disabled')
		);
		user.newResult(parseInt(clicksAmount.textContent.trim()));
		alert('You clicked ' + parseInt(clicksAmount.textContent.trim()) + ' times!');
	}, 5000);
});

clicker.addEventListener('click', () => {
	let num = parseInt(clicksAmount.textContent.trim());
	clicksAmount.textContent = num + 1;
});

bestResult.addEventListener('click', () => {
	user = new CreateGame(user.currentUser[0].username);
	alert('Your best result is: ' + user.currentUser[0].highestScore);
});

bestResultAllTime.addEventListener('click', () => {
	user = new CreateGame(user.currentUser[0].username);
	let thBestRes =
		parseInt(user.bestResultForAllTime) > 0 ? user.bestResultForAllTime : 0;
	alert('The best result for the whole time is: ' + thBestRes);
});

clearBestResult.addEventListener('click', () => {
	clicksAmount.textContent = '0';
	user = new CreateGame(user.currentUser[0].username);
	user.clearBestResult();
});

clearBestResultAllTime.addEventListener('click', () => {
	clicksAmount.textContent = '0';
	user = new CreateGame(user.currentUser[0].username);
	user.clearBestResultAllTime();
});



class CreateGame {
	constructor(username) {
		this.users = JSON.parse(localStorage.getItem('users')) || [];
		this.userExists =
			this.users.filter((user) => user.username === username).length === 1;
		this.currentUser = this.userExists
			? this.users.filter((user) => user.username === username)
			: [{
						username: username,
						previousResult: 0,
						highestScore: 0
					}];
		this.theBestUser =
			this.users.length > 0
				? this.users.filter(
					(user) => user.highestScore === this.users.map((user) =>
				user.highestScore).max()): 
				null;
		this.bestResult = this.userExists ? this.currentUser[0].highestScore : 0;
		this.bestResultForAllTime = this.theBestUser
			? this.theBestUser[0].highestScore + ' by ' + this.theBestUser[0].username
			: 0;
		this.updateDB = function () {
			localStorage.setItem('users', JSON.stringify(this.users));
			this.users = JSON.parse(localStorage.getItem('users'));
		};
	}
	newUser() {
		if (!this.userExists) {
			this.users.push(this.currentUser[0]);
			this.updateDB(this.users);
		}
	}
	newResult(result = 0) {
		let _user = this.users[this.users.indexOf(this.currentUser[0])];
		_user.previousResult = result;
		if (_user.highestScore < result) {
			_user.highestScore = result;
		}
		this.updateDB(this.users);
	}
	clearBestResult() {
		let _user = this.users[this.users.indexOf(this.currentUser[0])];
		_user.highestScore = 0;
		this.updateDB(this.users);
	}

	clearBestResultAllTime() {
		this.users.forEach((_user) => {
			_user.highestScore = 0;
		});
		this.updateDB();
		alert(
			'The best result for the whole all time is: ' +
				this.users.map((user) => user.highestScore).max()
		);
	}
}


Array.prototype.max = function () {
	return Math.max.apply(null, this);
};